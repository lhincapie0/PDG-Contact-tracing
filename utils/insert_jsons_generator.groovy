/**
* This file generates the insert statements for 
*/
import groovy.io.FileType
import java.io.File 

/**
* This variables are for configure for your own local data
* txt_generated_name = name of the file that will contains the sql script
* dbName = name of the db to insert the data
* json_files_folder = location where the aerohive jsons are located.
*/
def txt_generated_name = "test_december2"
def dbName = "public.pruebasdiciembre2"
def json_files_folder = "15Dic"

def list = []

def dir = new File(json_files_folder)
dir.eachFileRecurse (FileType.FILES) { file ->
  list << file
}

def statement = ''
list.each {
   item -> 
      File fh1 = new File('newUbicaciones.csv')
      def data = fh1.getText('UTF-8')
      statement =  statement + 'INSERT INTO '+dbName +' ("'+'data'+'"'+") VALUES('"+data+"'); \n"   
}

  new File("${txt_generated_name}.txt").withWriter('utf-8') { 
         writer -> writer.writeLine statement 
  }  

