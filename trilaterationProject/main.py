import databaseService
import trilateration

import numpy as np
import matplotlib.pyplot as plt
import psycopg2
import pandas as pd
import sys
import datetime
from psycopg2 import connect
from psycopg2 import OperationalError, errorcodes, errors
accessPoints = []
accessPointsMacs = []
observers = []
logInformation = []

## Current available information

def handleDeviceInformation(deviceMac, accessPoints, accessPointsMacs, distanceMethod):
  dev = databaseService.fetchDeviceObserversInfo(deviceMac)
  observers = trilateration.saveObserversInfo(dev)
  momentsProfile = trilateration.handleTimeRange(observers, deviceMac)
  records = trilateration.trilat(deviceMac, momentsProfile, observers, accessPoints, accessPointsMacs,distanceMethod)
  logInformation.append(records)

def saveResults():
  informationToLog = []
  for rec in logInformation:
    for i in rec:
      informationToLog.append(i)
  trilateration.printResultsMethods(informationToLog)

def locate(distanceMethod, device_mac, date ):
  aps = databaseService.fetchAccessPointsInfo()
  accessPoints = trilateration.saveAccessPointsInfo(aps)
  apsMacs = databaseService.fetchAccessPointsMacInfo()
  for ap in apsMacs:
    for info in ap:
        accessPointsMacs.append(trilateration.handleAPMacInfo(info))
  handleDeviceInformation(device_mac, accessPoints, accessPointsMacs,distanceMethod)
  print(logInformation)
  saveResults()

# 2 lineal method
# 3 exponential method
# init(1)
