
available_dates = []
available_macs = []

def setup():
  load_available_dates()
  load_available_macs()

def load_available_dates():
  with open("available_dates.csv") as f:
      lis = [line.split() for line in f]        # create a list of lists
      for i, x in enumerate(lis):              #print the list items 
          print(i)
          available_dates.append(x[0])

def load_available_macs():
  for date in available_dates:
    with open(date+"-macs.csv") as f:
      lis = [line.split() for line in f]
      macs = []        # create a list of lists
      for i, x in enumerate(lis):             #print the list items 
          macs.append(x[0])
    available_macs.append({
        "date": date,
        "available_macs": macs
    })
  print(available_macs)

def get_available_macs(date):
  for item in available_macs:
    if item.get('date') == date:
      return item.get('available_macs')