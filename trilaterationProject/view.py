import PySimpleGUI as sg
import main
import setup

# This is a basic GUI to test trilateration method
# To be able to run the method with no problems, some validation are being made to check input values are avaiable for study in the current db
distance_methods = ['Log Dist Shadow', 'Lineal Model','Exponential Model']
errors = 'error'


def verify_mac(device_mac, date):
  print("verifying mac")
  valid_mac = False
  available_macs = setup.get_available_macs(date)
  if device_mac in available_macs:
    valid_mac = True
  else:
    sg.popup('La MAC ingresada no fue estudiada en esta fecha \nLas dispositivos fueron: '+str(available_macs))
    valid_mac = False

  return valid_mac

def init():
  setup.setup()

  main_layout = [
            [sg.Text("Introduzca la MAC del dispositivo que desea ubicar")],
            [sg.Input(size=(45, 2))],
            [sg.Text("Escoja el método con el cual se calculará la distancia del AP al dispositivo",size=(50, 2))],
            [sg.Listbox(values=distance_methods, size=(45, 6), key='method', enable_events=False)],
            [sg.Input(key='date', size=(45,1)), sg.CalendarButton('Fecha',  target='date', format='%d-%m-%y', default_date_m_d_y=(1,None,2020), )],
            [sg.Text("Nota: Todos los campos son requeridos para llevar acabo la ubicación del dispositivo")],
            [sg.Text('La fecha escogida no se encuentra en el rango del estudio \nLas fechas estudiadas son: ' + str(setup.available_dates), key='date-error', visible=False)],
            [sg.Text('Por favor escoja un método para calcular la distancia', key='method-error', visible=False)],
            [sg.Text('Es necesario indicar la MAC del dispositivo que desea ubicar', key='mac-error', visible=False)],
            [sg.Button('Ubicar dispositivo')] ]
  # Create the window
  window = sg.Window('Ubicación de dispositivos mediante método de Trilateración', main_layout)
  # Display and interact with the Window
  event, values = window.read()

  # Do something with the information gathered


  while True:
    event, values = window.read()

    if event == 'Ubicar dispositivo':
      date = values['date']
      device_mac = values[0]
      valid_date = False
      valid_method = False
      valid_mac = False

      ## VALIDATIONS
      if date not in setup.available_dates:
        window['date-error'].Update(visible=True)
        valid_date = False
      else:
        window['date-error'].Update(visible=False)
        valid_date = True
      if values['method'] == []:
        window['method-error'].Update(visible=True)
        valid_method = False
      else:
        window['method-error'].Update(visible=False)
        valid_method = True
      if device_mac == '':
        window['mac-error'].Update(visible=True)
        valid_mac = False
      else:
        window['mac-error'].Update(visible=False)
      if valid_date:
        valid_mac = verify_mac(device_mac, date)

      if valid_date and valid_mac and valid_method:
        print("bien se puede hacer")
        distance_method = distance_methods.index(values['method'][0]) + 1
        print("[info] Executing trilateration with distance method: "+values['method'][0])
        main.locate(distance_method, device_mac, date)
    if event in (sg.WIN_CLOSED, 'Exit'):
      break

init()



