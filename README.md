# PDG-Contact-tracing

### Preconditions:
Database should be created with the Webhook data

---

#### Help importing Webhook jsons into database:

A groovy file has been created to generate insert statements for the data table. To use this read the documentation in utils/insert_jsons_generator.groovy

- compile groovy file: `groovyc utils/insert_jsons_generator.groovy`
- run groovy file: `groovy utils/insert_jsons_generator.groovy`

Note make sure you install groovy correctly. Installation guide: https://groovy-lang.org/install.html

## To run trilaterationProject

`cd trilaterationProject`
- install requirements: `pip3 install -r requirements.txt`
- run project with main view: `python3 view.py`

## Utils

To generate project requirements run:  `pip3 freeze > requirements.txt`

### Config files:

As this is a iterative project, there are some csv files created, to be able to increase the project scope.

* trilaterationProject/avaliable_date.csv: Contains experiment dates, new dates can be added here with the precondition that the a database with the jsons for the day has already been created. Otherwise it will be useless.
* trilaterationProject/${date}-macs.csv: Contains the devices that were studied in the prefixed date.
