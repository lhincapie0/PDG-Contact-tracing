/*CREATE TABLE example (
        id serial NOT NULL PRIMARY KEY,
        data json NOT NULL
);*/
/*CREATE TABLE apsfiles (
        id serial NOT NULL PRIMARY KEY,
        data jsonb NOT NULL
);*/

CREATE TABLE apsinfo (
        id serial NOT NULL PRIMARY KEY,
        "Host Name" varchar,
		"MGT IP Address" varchar,
		"MAC" varchar,
		"Location" varchar,
		"Model" varchar,
		"IQ Engine" varchar,
		"Policy" varchar,
		"WiFi0 Channel" varchar,
		"WiFi0 Power" varchar,
		"WiFi1 ChanneL" varchar,
		"WiFi1 Power" varchar,
		"MGT VLAN" varchar,
		"WiFi0 Radio" varchar,
		"WiFi1 Radio" varchar
);


