import groovy.io.FileType
import java.io.File 


def list = []
def folder = "LOAD_DATA"

def dir = new File("resources")
dir.eachFileRecurse (FileType.FILES) { file ->
  list << file
}

def statement = ''
list.each {
   item -> 
      File fh1 = new File(item.path)
      def data = fh1.getText('UTF-8')
      statement =  statement + 'INSERT INTO public.example ("'+'data'+'"'+") VALUES('"+data+"'); \n"   
}

  new File("${folder}.txt").withWriter('utf-8') { 
         writer -> writer.writeLine statement 
  }  
